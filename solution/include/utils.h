#ifndef UTILS_H
#define UTILS_H
#include "image.h"
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INCORRECT_FILE
};
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INCORRECT_FILE
};
uint64_t compute_padding(struct image const* img);
#endif
