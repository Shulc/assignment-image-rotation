#ifndef IMG_ROTATION_H
#define IMG_ROTATION_H
#include "bmp.h"

struct image rotate( struct image const source );

#endif
