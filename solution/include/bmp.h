#ifndef BMP_H
#define BMP_H
#include "utils.h"
#include <stdio.h>
struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
struct bmp_header bmp_header_from_image(struct image const* img);
struct image image_from_bmp_header(struct bmp_header bmpHeader);
enum read_status from_bmp( FILE* in, struct image* img);
enum write_status to_bmp( FILE* out, struct image const* img );
struct image_container read_bmp(const char *file);
enum write_status write_bmp(const char *file, struct image img);
#endif
