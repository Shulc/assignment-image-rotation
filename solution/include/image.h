#ifndef IMAGE_H
#define IMAGE_H
#ifndef BMP_H
#define BMP_H
#endif
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
struct pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct image_container{
    struct image img;
    bool valid;
};
struct image init_image(uint64_t width, uint64_t height);
void free_img(struct image img);
#endif


