#include "img_rot.h"


int main(int argc, char** argv) {
  (void)argc;
    (void)argv;

    struct image_container img_c = read_bmp(argv[1]);
    if (!(img_c.valid)){
        return READ_INCORRECT_FILE;
    }

    struct image rotated_img = rotate(img_c.img);
    write_bmp(argv[2], rotated_img);
    free_img(img_c.img);
    free_img(rotated_img);
    return 0;
}
