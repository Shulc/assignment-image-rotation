#include "../include/img_rot.h"


struct image rotate( struct image const source ){
    //swap width and height for new image
    struct image img = init_image(source.height, source.width);
    for (size_t i=0; i<source.height; i++){
        for(size_t j = 0; j<source.width; j++){
            (img.data)[(img.width)*(img.height-j-1)+i] = source.data[source.width *i+j];
        }
    }
    return img;
}

