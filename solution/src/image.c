#include "../include/image.h"
struct image init_image(uint64_t width, uint64_t height){
    struct image img = (struct image){.width=width, .height=height};
    img.data= (struct pixel *) malloc((height)*(width)* sizeof(struct pixel));
    return img;
}
void free_img(struct image img){
    free(img.data);
}
