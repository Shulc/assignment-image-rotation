#include "../include/bmp.h"
struct bmp_header bmp_header_from_image(struct image const* img){
    return (struct bmp_header) {
            .bfType = 0x4D42,
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = img->width*sizeof(struct pixel)*img->height,
            .bfileSize = img->width*sizeof(struct pixel)*img->height+ 54,
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}
struct image image_from_bmp_header(struct bmp_header bmpHeader){
    return init_image(bmpHeader.biWidth, bmpHeader.biHeight);
}
bool bmp_header_is_valid(const struct bmp_header header){
    if (header.bfType != 0x4D42){
        return false;
    }
    return true;
}
enum read_status from_bmp( FILE* in, struct image* img) {
    struct bmp_header header;

    if( !fread( &header, sizeof( struct bmp_header ), 1, in )) {
        return READ_INVALID_HEADER;
    }

    if(!bmp_header_is_valid(header)) {
        return READ_INCORRECT_FILE;
    }

    *img = (image_from_bmp_header(header));
    struct pixel* cur_str = img->data+img->width*(img->height-1);

    for (size_t i = img->height; i != 0; i-=1) {
        if( !fread( cur_str, img->width * sizeof(struct pixel), 1, in )) {
            free(img->data);
            return READ_INVALID_BITS;
        }
        cur_str-=img->width;
        uint8_t padding[4];
        if (!fread(padding, compute_padding(img), 1, in)){
            return READ_INVALID_BITS;
        }
    }
    //err("hui\n");
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = bmp_header_from_image(img);

    if( !fwrite( &header, sizeof(struct bmp_header), 1, out )) {
        return WRITE_ERROR;
    }
    struct pixel* cur_str = img->data+img->width*(img->height-1);
    for( uint64_t i = img->height; i != 0; i-=1) {
        if ( !fwrite(cur_str, (img->width) * sizeof(struct pixel), 1, out) ) {
            return WRITE_ERROR;
        }
        cur_str-=img->width;
        if (fseek(out, (int64_t)compute_padding(img),SEEK_CUR)){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
struct image_container read_bmp(const char *file){
    FILE *in = fopen(file, "rb");
    struct image_container img_c=(struct image_container){.valid=false};

    if (!in){
        return img_c;
    }

    if (from_bmp(in, &(img_c.img))!=0){
        return img_c;
    }

    img_c.valid=true;
    return img_c;
}
enum write_status write_bmp(const char *file, const struct image img) {
    FILE *out = fopen(file, "wb");
    if (!out) {
        return WRITE_INCORRECT_FILE;
    }
    enum write_status status = to_bmp( out, &img);
    fclose(out);
    return status;
}
